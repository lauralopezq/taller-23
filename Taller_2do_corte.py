from math import *
from graphics import *

ventana= GraphWin("Simulador semiparabolico", 800, 600)
ventana.setCoords(0,0,800,600)
ventana.getMouse()
ventana.close()
t=0
g=9.8
y=12
Yo=150
Vo=280
a=radians(45)

while y>=0:
    x=Vo*t
    y=Yo+(1/2)(g*t*2)
    print(x,y)
    Semi_Circulo=Circle(Point(x,y),2)
    Semi_Circulo.setFill ("Red")
    Semi_Circulo.draw(ventana)
    t+=0.01
print (t)